#!/bin/bash
FILE=$1
while read -r LINE; do
    go run *.go "$LINE"
done < "$FILE"
