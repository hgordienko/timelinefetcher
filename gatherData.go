package main

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"github.com/ChimeraCoder/anaconda"
	// "bytes"
	// "io/ioutil"
	// "log"
	"gopkg.in/mgo.v2"
	// "math"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	// b64 "encoding/base64"
	// "encoding/json"
)

type BearerToken struct {
	AccessToken string `json:"access_token"`
}

var (
	req        *http.Request
	DBsession  *mgo.Session
	tweetsChan chan anaconda.Tweet = make(chan anaconda.Tweet)
	client     *http.Client        = &http.Client{}
	b          BearerToken
)

var APIobject = struct {
	sync.RWMutex // anonymous field i.e. APIobject is a mutex
	tApi         *anaconda.TwitterApi
}{}

func panicAttack(err error) {
	if err != nil {
		panic(err)
	}
}

func initTwitterApp(ConsumerKey, ConsumerKeySecret, AccessToken, AccessTokenSecret string) {
	// consumer keys are for the application-only auth
	anaconda.SetConsumerKey(ConsumerKey)
	anaconda.SetConsumerSecret(ConsumerKeySecret)

	//NewTwitterApi takes an user-specific access token and secret and returns a TwitterApi struct for that user.
	APIobject.Lock()
	APIobject.tApi = anaconda.NewTwitterApi(AccessToken, AccessTokenSecret) //AccessToken, AccessTokenSecret
	APIobject.tApi.ReturnRateLimitError(true)                               // rate limit for the user
	APIobject.Unlock()

}

// handling error check
func fileInput(fileName string) []string {

	file, err := os.Open(fileName)

	if err != nil {
		fmt.Printf("Error opening file %s.\nError message: %s \n", fileName, err)
		return nil
	}

	output, err := readLinesFromFile(file)

	if err != nil {
		fmt.Printf("Error reading file %s.\nError message: %s \n", fileName, err)
		return nil
	}

	return output
}

func readLinesFromFile(file *os.File) ([]string, error) {

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines, scanner.Err()

}

func passTweets(tweets []anaconda.Tweet, twChannel chan anaconda.Tweet) {

	for {
		var (
			first   anaconda.Tweet
			auxChan chan anaconda.Tweet
		)

		if len(tweets) > 0 {
			first = tweets[0]
			auxChan = twChannel

			select {
			case auxChan <- first:
				tweets = tweets[1:]

			}
		} else {
			return
		}
	}
}

// if rate limit has been hit, sleep until the next rate limit window
func rateLimitHit(err error) (bool, time.Duration) {
	if aerr, ok := err.(*anaconda.ApiError); ok {
		if isRateLimitError, nextWindow := aerr.RateLimitCheck(); isRateLimitError {
			fmt.Printf("Rate limit exceeded. Next window starts in ")
			fmt.Println(nextWindow.Sub(time.Now()))
			return true, nextWindow.Sub(time.Now())
		}
	}
	return false, time.Duration(0)
}

func getTweets(screen_name string, form url.Values) ([]anaconda.Tweet, error) {

	form.Set("screen_name", screen_name)
	APIobject.RLock()
	tweets, err := (*(APIobject.tApi)).GetUserTimeline(form)
	APIobject.RUnlock()
	fmt.Printf("FOUND: %d QUERY: %s %s", len(tweets), screen_name, form)
	return tweets, err
}

func search(screen_name string, d time.Duration) {
	vFresh := url.Values{}
	//vFresh.Set("since_id", "0")
	vFresh.Set("count", "200")

	go launchSearch(screen_name, d, vFresh)
}

func writeResToDB(dbName string, colName string, d time.Duration) {

	// TIMEOUT is set here
	timeout := time.After(d)
	for {
		var (
			twChan chan anaconda.Tweet
			err    error
		)

		twChan = tweetsChan

		select {
		case tweet := <-twChan:

			panicAttack(err)

			c := DBsession.DB(dbName).C(colName)
			err = c.Insert(tweet)

		// if it's time to shut down
		// timeout - for debug
		case <-timeout:

			fmt.Println("DBA: timed out ")
			return
		default:
			time.Sleep(time.Millisecond)
		}
	}
}

func updateURLValues(v url.Values, field string, newLimit int64) url.Values {

	fieldVal := v.Get(field)
	if fieldVal == "" {
		fmt.Printf(" WARN. No such field: %s\n", field)
		return v
	}

	prevLimit, err := strconv.ParseInt(fieldVal, 10, 64)

	if err != nil {
		fmt.Println(err)
		return v
	}

	switch field {
	case "max_id":
		{
			// this param is inclusive => have to decrease newLimit by one
			if newLimit < prevLimit {
				v.Set(field, strconv.FormatInt(newLimit-1, 10))
			}
			return v
		}
	case "since_id":
		{
			if newLimit > prevLimit {
				v.Set(field, strconv.FormatInt(newLimit, 10))
			}
			return v
		}
	default:
		{
			return v
		}

	}
}

func launchSearch(screen_name string, d time.Duration, formFresh url.Values) {

	timeout := time.After(d)
	var searchResult []anaconda.Tweet
	select {
	case <-timeout:
		fmt.Println("Search timeout exceeded. Stoping search...")
		return
	default:
		tweets, err := getTweets(screen_name, formFresh)
		searchResult = append(searchResult, tweets...)

		if len(searchResult) > 0 {
			go passTweets(searchResult, tweetsChan)
		}

		if hit, dur := rateLimitHit(err); hit {
			fmt.Println("Rate limit hit for user-auth")
			fmt.Println(dur)
			return
		}
	}
}

func main() {
	var err error

	keyPath := "/home/lego/Go"  //os.Args[1]
	server := "127.0.0.1:27017" //os.Args[2]
	dbName := "TwitterData"     //os.Args[3]

	if len(os.Args) < 2 {
		fmt.Println("Usage: <screen_name>")
		return
	}

	keys := fileInput(keyPath + "/" + "keys.txt")
	keysSecret := fileInput(keyPath + "/" + "keysSecret.txt")
	name := os.Args[1]

	//================================= INITIALIZING TWITTER APP =======================================
	workTime := time.Second * 20

	initTwitterApp(keys[0], keysSecret[0], keys[1], keysSecret[1])

	DBsession, err = mgo.Dial(server)

	if err != nil {
		fmt.Printf("Error connecting to db server %s.\nError message: %s \n", server, err)
	}
	fmt.Printf("%t", DBsession == nil)
	panicAttack(err)
	defer DBsession.Close()

	DBsession.SetMode(mgo.Monotonic, true)

	go writeResToDB(dbName, "A"+name, workTime)

	//for _, qr := range screenNames {
	go search(name, workTime)
	//}

	time.Sleep(workTime)
	me, err := (*APIobject.tApi).GetSelf(nil)
	panicAttack(err)
	fmt.Printf("Bye, %s\n", me.ScreenName)
}
